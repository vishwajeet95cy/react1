import React from 'react';
import './Form.css'

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      age: null,
      email: '',
      password: '',
      state: ''
    };
  }
  changeData = (event) => {
    let name = event.target.name;
    let val = event.target.value;
    this.setState({ [name]: val })
  }
  render() {
    return <>
      <form>
        <h1 className="head">Simple Form Using React</h1>
        <h1 className="data">Hello {this.state.name} {this.state.age} {this.state.email} {this.state.state}</h1>
        <div className='form-group'>
          <label>Enter your Name:</label>
          <input type="text" name="name" onChange={this.changeData}></input>
        </div>
        <div className='form-group'>
          <label>Enter your Age:</label>
          <input type="text" name="age" onChange={this.changeData}></input>
        </div>
        <div className='form-group'>
          <label>Enter your Email:</label>
          <input type="text" name="email" onChange={this.changeData}></input>
        </div>
        <div className='form-group'>
          <label>Enter your Password:</label>
          <input type="password" name="password" onChange={this.changeData}></input>
        </div>
        <div className='form-group'>
          <label>Select State</label>
          <select name="state" onChange={this.changeData}>
            <option value=''>Select State</option>
            <option value='Bihar'>Bihar</option>
            <option value='West Bengal'>West Bengal</option>
            <option value='Maharastra'>Maharastra</option>
            <option value='Haryana'>Haryana</option>
          </select>
        </div>
        <button className="btn" type="button">Submit</button>
      </form>
    </>;
  }
}

export default Form;
